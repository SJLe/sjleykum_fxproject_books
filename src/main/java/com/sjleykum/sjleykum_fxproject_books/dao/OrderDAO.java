package com.sjleykum.sjleykum_fxproject_books.dao;

import com.sjleykum.sjleykum_fxproject_books.model.BookModel;
import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;
import com.sjleykum.sjleykum_fxproject_books.model.OrderModel;

import java.util.List;

/**
 * OrderDao
 *
 * @author Leykum, Salman Jona
 * @version 03.12.2021
 */
public interface OrderDAO {

    void addOrder(CustomerModel customerModel);

    void addItemToOrder(BookModel bookModel, OrderModel orderModel);

    void readCustomerOrders(CustomerModel customerModel);

    void readOrderItems(OrderModel orderModel);

    void deleteItemFromOrder(BookModel bookModel, OrderModel orderModel);

    void deleteCustomerOrder(OrderModel orderModel);

    List<OrderModel> getOrderModelList(CustomerModel customerModel);

    List<BookModel> getOrderItemList(OrderModel orderModel);
}
