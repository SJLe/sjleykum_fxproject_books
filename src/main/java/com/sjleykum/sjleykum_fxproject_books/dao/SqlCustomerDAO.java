package com.sjleykum.sjleykum_fxproject_books.dao;

import com.sjleykum.sjleykum_fxproject_books.CustomerGender;
import com.sjleykum.sjleykum_fxproject_books.model.DbConnection;
import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * SqlCustomerDAO
 *
 * @author Leykum, Salman Jona
 * @version 03.12.2021
 */
public class SqlCustomerDAO implements CustomerDAO {
    private Connection connection;
    private List<CustomerModel> customerModelList;
    private AddressDAO addressDAO = new SqlAddressDAO();

    public SqlCustomerDAO() {
        try {
            this.connection = DbConnection.getInstance();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public List<CustomerModel> getCustomerModelList() {
        return customerModelList;
    }


    @Override
    public int addCustomer(CustomerModel customerModel) {
        int generatedKey = 0;
        String sqlInsert = "INSERT INTO customers (firstname, lastname, gender) VALUES (?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, customerModel.getFirstname());
            pstmt.setString(2, customerModel.getLastname());
            pstmt.setString(3, customerModel.getCustomerGender());
            pstmt.executeUpdate();
            ResultSet resultSet = pstmt.getGeneratedKeys();
            while (resultSet.next()) {
                generatedKey = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return generatedKey;
    }


    @Override
    public void readAllCustomers() {
        customerModelList = new ArrayList<>();
        String sqlQuery = "SELECT id_customer, firstname, lastname, gender FROM customers";
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(sqlQuery)) {
                addResultSetToCustomerModelList(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addResultSetToCustomerModelList(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            CustomerModel customerModel = new CustomerModel(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    CustomerGender.valueOf(resultSet.getString(4))
            );
            customerModel.setBillingAddress(addressDAO.getCustomerBillingAddress(customerModel));
            customerModel.setShippingAddress(addressDAO.getCustomerShippingAddress(customerModel));

            customerModelList.add(customerModel);
        }
    }

    @Override
    public void updateCustomer(CustomerModel customerModel, CustomerModel newCustomer) {
        String sqlQuery = "UPDATE customers SET firstname = ?, lastname = ?, gender = ? WHERE id_customer = ?";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setString(1, newCustomer.getFirstname());
            pstmt.setString(2, newCustomer.getLastname());
            pstmt.setString(3, newCustomer.getCustomerGender());
            pstmt.setInt(4, customerModel.getId_customer());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCustomer(CustomerModel customerModel) {
        String sqlQuery = "DELETE FROM customers WHERE id_customer = ?";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, customerModel.getId_customer());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searchCustomer(String searchString) {
        customerModelList = new ArrayList<>();
        searchString = "%" + searchString + "%";
        String sqlQuery = "SELECT id_customer, firstname, lastname, gender FROM customers " +
                "WHERE lastname LIKE ?" +
                "OR firstname like ?" +
                "OR id_customer like ?";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setString(1, searchString);
            pstmt.setString(2, searchString);
            pstmt.setString(3, searchString);
            pstmt.execute();
            ResultSet resultSet = pstmt.getResultSet();
            addResultSetToCustomerModelList(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
