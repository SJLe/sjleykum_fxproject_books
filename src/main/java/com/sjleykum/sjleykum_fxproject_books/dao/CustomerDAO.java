package com.sjleykum.sjleykum_fxproject_books.dao;

import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;

import java.util.List;

public interface CustomerDAO {

    List<CustomerModel> getCustomerModelList();

    int addCustomer(CustomerModel customerModel);

    void readAllCustomers();

    void searchCustomer(String searchString);

    void updateCustomer(CustomerModel customerModel, CustomerModel newCustomerModel);

    void deleteCustomer(CustomerModel customerModel);

}