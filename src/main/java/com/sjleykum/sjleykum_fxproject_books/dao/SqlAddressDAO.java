package com.sjleykum.sjleykum_fxproject_books.dao;

import com.sjleykum.sjleykum_fxproject_books.model.DbConnection;
import com.sjleykum.sjleykum_fxproject_books.model.AddressModel;
import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * SqlAddressDAO
 *
 * @author Leykum, Salman Jona
 * @version 06.12.2021
 */
public class SqlAddressDAO implements AddressDAO {
    private Connection connection;
    private List<AddressModel> addressModelList;

    public SqlAddressDAO() {
        try {
            this.connection = DbConnection.getInstance();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addAddress(AddressModel addressModel, int pk) {

        String sqlInsert = "INSERT INTO address (FK_customer_id ,street, zipCode, city, isShippingAddress)  VALUES (?,?,?,?,?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlInsert)) {
            pstmt.setInt(1, pk);
            pstmt.setString(2, addressModel.getSteet());
            pstmt.setString(3, addressModel.getZipcode());
            pstmt.setString(4, addressModel.getCity());
            pstmt.setBoolean(5, addressModel.isShippingAddress());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAddress(AddressModel addressModel, AddressModel newaddressModel, CustomerModel customerModel, boolean isShippingAddress) {
        String sqlQuery = "UPDATE address SET street = ?, zipCode = ?, city = ?, isShippingAddress = ?  WHERE FK_customer_id = ? AND isShippingAddress = ?";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setString(1, newaddressModel.getSteet());
            pstmt.setString(2, newaddressModel.getZipcode());
            pstmt.setString(3, newaddressModel.getCity());
            pstmt.setBoolean(4, isShippingAddress);
            pstmt.setInt(5, customerModel.getId_customer());
            pstmt.setBoolean(6, addressModel.isShippingAddress());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void readCustomerAddress(CustomerModel customerModel) {
        addressModelList = new ArrayList<>();
        String sqlQuery = "SELECT street, city, zipCode, isShippingAddress FROM address where FK_customer_id = ?";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, customerModel.getId_customer());
            pstmt.executeQuery();
            ResultSet resultSet = pstmt.getResultSet();
            while (resultSet.next()) {
                addressModelList.add(new AddressModel(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getBoolean(4)
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public AddressModel getCustomerBillingAddress(CustomerModel customerModel) {
        String sqlQuery = "SELECT street, zipCode, city, isShippingAddress FROM address where FK_customer_id = ? AND isShippingAddress = 0";
        return newAddress(customerModel, sqlQuery);
    }

    @Override
    public AddressModel getCustomerShippingAddress(CustomerModel customerModel) {
        String sqlQuery = "SELECT street, zipCode, city, isShippingAddress FROM address where FK_customer_id = ? AND isShippingAddress = 1";
        return newAddress(customerModel, sqlQuery);
    }

    private AddressModel newAddress(CustomerModel customerModel, String sqlQuery) {
        AddressModel addressModel = null;
        try (PreparedStatement pstmt = connection.prepareStatement(sqlQuery)) {
            pstmt.setInt(1, customerModel.getId_customer());
            pstmt.executeQuery();
            ResultSet resultSet = pstmt.getResultSet();
            while (resultSet.next()) {
                addressModel = new AddressModel(resultSet.getString(1), resultSet.getString(2), resultSet.getString(3), resultSet.getBoolean(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return addressModel;
    }
}
