package com.sjleykum.sjleykum_fxproject_books.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.sjleykum.sjleykum_fxproject_books.model.BookModel;
import org.json.*;


/**
 * WebApiBookDAO
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class JsonWebApiBookDAO implements BookDAO{

    private List<BookModel> queryResult;


    @Override
    public List<BookModel> getQueryResult() {
        return null;
    }

    @Override
    public void searchAuthor(String author) {

    }

    @Override
    public List<BookModel> searchTitles(String title) throws JSONException {
        queryResult = new ArrayList<>();
        String jsonSting = "{}";
        try {
            jsonSting = getJSONfromURL("https://reststop.randomhouse.com/resources/titles?search=" + title);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject queryJsonObject = new JSONObject(jsonSting);
        fillQueryResult(queryJsonObject);
        return queryResult;
    }

    private void fillQueryResult(JSONObject queryJsonObject) throws JSONException{
        if (queryJsonObject.has("title")) {
            try {
                JSONArray jsonArray = queryJsonObject.getJSONArray("title");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    queryResult.add(new BookModel(
                            jsonObject.getString("titleshort"),
                            jsonObject.getString("titleweb"),
                            jsonObject.getString("author"),
                            jsonObject.getString("authorweb"),
                            jsonObject.getDouble("priceusa"),
                            jsonObject.getString("isbn"),
                            jsonObject.getString("formatname"),
                            jsonObject.getString("@uri"),
                            jsonObject.getString("keyword")
                    ));
                }
            } catch (JSONException e) {
//                Alert alert = new Alert(Alert.AlertType.ERROR, "Fehler");
//                alert.setContentText(e.getMessage());
//                alert.show();
            }

        }
    }


    private static String readResponse(HttpURLConnection con) throws IOException {
        InputStream inputStream = con.getInputStream();
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String row;
            while ((row = reader.readLine()) != null) {
                stringBuilder.append(row);
            }
        }
        return stringBuilder.toString();
    }

    public static String getJSONfromURL(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestMethod("GET");
        return readResponse(con);
    }
}
