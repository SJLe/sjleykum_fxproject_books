package com.sjleykum.sjleykum_fxproject_books.dao;

import com.sjleykum.sjleykum_fxproject_books.model.BookModel;
import com.sjleykum.sjleykum_fxproject_books.model.DbConnection;
import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;
import com.sjleykum.sjleykum_fxproject_books.model.OrderModel;
import javafx.scene.control.Alert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * SqlOrderDAO
 *
 * @author Leykum, Salman Jona
 * @version 03.12.2021
 */
public class SqlOrderDAO implements OrderDAO {
    private Connection connection;
    List<OrderModel> orderModelList;
    List<BookModel> orderItemList;

    public SqlOrderDAO() {
        try {
            this.connection = DbConnection.getInstance();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addOrder(CustomerModel customerModel) {
        String sqlInsert = "INSERT INTO customer_order (FK_id_customer) VALUES (?)";
        try (PreparedStatement pstmt = connection.prepareStatement(sqlInsert)) {
            pstmt.setInt(1, customerModel.getId_customer());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addItemToOrder(BookModel bookModel, OrderModel orderModel) {
        if (orderModel == null) {
            Alert noOrderSelectedAlter = new Alert(Alert.AlertType.WARNING, "Keine Bestellung ausgewählt");
            noOrderSelectedAlter.show();
        } else if (bookModel != null) {
            String sqlInsert = "INSERT INTO books (titleshort, author, priceusa, isbn, formatname, imgUrl, titleweb, authorweb, id_order, keyword) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?)";
            try (PreparedStatement pstmt = connection.prepareStatement(sqlInsert)) {
                pstmt.setString(1, bookModel.getTitleshort());
                pstmt.setString(2, bookModel.getAuthor());
                pstmt.setDouble(3, bookModel.getPriceusa());
                pstmt.setString(4, bookModel.getIsbn());
                pstmt.setString(5, bookModel.getFormatname());
                pstmt.setString(6, bookModel.getImgUrl());
                pstmt.setString(7, bookModel.getTitleweb());
                pstmt.setString(8, bookModel.getAuthorweb());
                pstmt.setInt(9, orderModel.getId_order());
                pstmt.setString(10,bookModel.getKeyword());
                pstmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void readCustomerOrders(CustomerModel customerModel) {
        orderModelList = new ArrayList<>();
        //todo delete
        if (customerModel != null) {
            String sqlQuery = "SELECT id_order, FK_id_customer  FROM customer_order " +
                    "WHERE FK_id_customer = " + customerModel.getId_customer();
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(sqlQuery)) {
                    while (resultSet.next()) {
                        orderModelList.add(new OrderModel(
                                resultSet.getInt(1),
                                resultSet.getInt(2)
                        ));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void readOrderItems(OrderModel orderModel) {
        orderItemList = new ArrayList<>();
        BookModel bookModel;
        if (orderModel != null) {
            //todo delete console log
            String sqlQuery = "SELECT titleshort, titleweb, author, authorweb, priceusa, isbn, formatname, imgUrl, keyword, pk " +
                    "FROM books WHERE id_order = " + orderModel.getId_order();
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(sqlQuery)) {
                    while (resultSet.next()) {
                        orderItemList.add(bookModel = new BookModel(
                                resultSet.getString(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getDouble(5),
                                resultSet.getString(6),
                                resultSet.getString(7),
                                resultSet.getString(8),
                                resultSet.getString(9)
                        ));
                        bookModel.setPk(resultSet.getInt(10));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deleteItemFromOrder(BookModel bookModel, OrderModel orderModel) {
        if (bookModel != null && orderModel != null) {
            String sqlDelete = "DELETE FROM books WHERE id_order = ? AND pk = ?";
            try (PreparedStatement pstmt = connection.prepareStatement(sqlDelete)) {
                pstmt.setInt(1, orderModel.getId_order());
                pstmt.setInt(2, bookModel.getPk());
                pstmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deleteCustomerOrder(OrderModel orderModel) {
        if (orderModel != null) {
            String sqlDelete = "DELETE FROM customer_order WHERE id_order = ?";
            try (PreparedStatement pstmt = connection.prepareStatement(sqlDelete)) {
                pstmt.setInt(1, orderModel.getId_order());
                pstmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //getter

    @Override
    public List<OrderModel> getOrderModelList(CustomerModel customerModel) {
        orderModelList = new ArrayList<>();
        readCustomerOrders(customerModel);
        return orderModelList;
    }

    @Override
    public List<BookModel> getOrderItemList(OrderModel orderModel) {
        readOrderItems(orderModel);
        return orderItemList;
    }
}
