package com.sjleykum.sjleykum_fxproject_books.dao;

import com.sjleykum.sjleykum_fxproject_books.model.BookModel;

import java.util.List;

public interface BookDAO {

    List<BookModel> getQueryResult();

    void searchAuthor (String author);

    List<BookModel> searchTitles (String author);

}
