package com.sjleykum.sjleykum_fxproject_books.dao;

import com.sjleykum.sjleykum_fxproject_books.model.AddressModel;
import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;

/**
 * AddressDAO
 *
 * @author Leykum, Salman Jona
 * @version 06.12.2021
 */
public interface AddressDAO {
    void addAddress(AddressModel addressModel, int pk);

    void updateAddress(AddressModel addressModel, AddressModel newAddressModel, CustomerModel customerModel, boolean isShippingAddress);

    void readCustomerAddress(CustomerModel customerModel);

    AddressModel getCustomerBillingAddress (CustomerModel customerModel);
    AddressModel getCustomerShippingAddress (CustomerModel customerModel);
}
