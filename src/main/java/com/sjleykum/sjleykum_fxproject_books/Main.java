package com.sjleykum.sjleykum_fxproject_books;

import com.sjleykum.sjleykum_fxproject_books.controller.ConnectionController;
import com.sjleykum.sjleykum_fxproject_books.controller.MainController;
import com.sjleykum.sjleykum_fxproject_books.model.DbConnection;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.sql.SQLException;

public class Main extends Application {

    public static void main(String[] args) {
        try {
            launch(args);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "unbekannter Fehler!");
            alert.setTitle("Unbekannter Fehler!");
            alert.setContentText(e.getMessage());
            alert.show();
        }
    }

    @Override
    public void start(Stage primaryStage) {

            ConnectionController connectionController = new ConnectionController();

            if (connectionController != null) {
                try {
                    DbConnection.getInstance();
                    MainController mainController = new MainController(primaryStage);
                } catch (SQLException e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText(e.getMessage());
                    alert.show();
                }
            }
    }

    @Override
    public void stop() throws Exception {
        DbConnection.closeConnection();
    }
}
