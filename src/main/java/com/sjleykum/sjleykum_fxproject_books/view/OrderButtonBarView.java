package com.sjleykum.sjleykum_fxproject_books.view;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 * OrderButtonBarView
 *
 * @author Leykum, Salman Jona
 * @version 02.12.2021
 */
public class OrderButtonBarView extends HBox {
    Button addOrderBtn = new Button("+ Bestellung");
    Button delOrderBtn = new Button("- Bestellung");
    Button pdfBtn = new Button("Rechnung");

    public OrderButtonBarView () {
        getStyleClass().add("ButtonBar");
        getChildren().addAll(addOrderBtn, delOrderBtn, pdfBtn);
    }

    public Button getAddOrderBtn() {
        return addOrderBtn;
    }

    public Button getPdfBtn() {
        return pdfBtn;
    }

    public Button getDelOrderBtn() {
        return delOrderBtn;
    }
}
