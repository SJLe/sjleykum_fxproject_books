package com.sjleykum.sjleykum_fxproject_books.view;

import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * CustomerView
 *
 * @author Leykum, Salman Jona
 * @version 02.12.2021
 */
public class CustomerListView extends VBox {
    private ListView<CustomerModel> listView;
    private ObservableList<CustomerModel> customerModelObservableList;


    public CustomerListView(){
        listView = new ListView<>();
        getChildren().addAll(listView);
    }

    public CustomerListView(List<CustomerModel> customerModelList) {
        customerModelObservableList = FXCollections.observableList(customerModelList);
        listView = new ListView<>(customerModelObservableList);
        getChildren().add(listView);
    }

    public ListView<CustomerModel> getListView() {
        return listView;
    }
}
