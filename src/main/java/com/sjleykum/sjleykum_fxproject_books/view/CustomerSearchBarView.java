package com.sjleykum.sjleykum_fxproject_books.view;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * CustomerSearchBarView
 *
 * @author Leykum, Salman Jona
 * @version 02.12.2021
 */
public class CustomerSearchBarView extends HBox {
    private Button customerSearchBtn = new Button("Suchen");
    private TextField customerSearchTextField = new TextField();

    public CustomerSearchBarView() {
        getStyleClass().add("ButtonBar");
        getChildren().addAll(customerSearchTextField,customerSearchBtn);
    }

    public Button getCustomerSearchBtn() {
        return customerSearchBtn;
    }

    public TextField getCustomerSearchTextField() {
        return customerSearchTextField;
    }
}
