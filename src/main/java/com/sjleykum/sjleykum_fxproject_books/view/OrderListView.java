package com.sjleykum.sjleykum_fxproject_books.view;

import com.sjleykum.sjleykum_fxproject_books.model.OrderModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * OrderListView
 *
 * @author Leykum, Salman Jona
 * @version 02.12.2021
 */
public class OrderListView extends VBox {
    ListView<OrderModel> listView;

    public  OrderListView () {
        listView = new ListView<>();
        getChildren().add(listView);
    }

    public OrderListView (List<OrderModel> orderModelList){
        ObservableList<OrderModel> orderModelObservableList = FXCollections.observableList(orderModelList);
        listView = new ListView<>(orderModelObservableList);
        getChildren().add(listView);
    }

    public ListView<OrderModel> getListView() {
        return listView;
    }
}
