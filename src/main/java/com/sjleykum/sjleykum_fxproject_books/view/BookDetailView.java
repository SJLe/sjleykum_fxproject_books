package com.sjleykum.sjleykum_fxproject_books.view;

import com.sjleykum.sjleykum_fxproject_books.model.BookModel;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * BookdetailView
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class BookDetailView extends VBox {
    TabPane tabPane = new TabPane();

    public BookDetailView(BookModel bookModel) {
        setId("BookDetailView");
        getChildren().add(topHbox(bookModel));
        getChildren().add(bottomTabPane(bookModel));
    }

    public BookDetailView() {
        setId("BookDetailView");
        getChildren().add(topHboxEmpty());
        getChildren().add(bottomTabPaneEmpty());
    }

    private Node topHbox(BookModel bookModel) {
        return new HBox(topVboxleft(), topVboxRight(bookModel));
    }

    private Node topHboxEmpty() {
        return new HBox(topVboxleft());
    }

    private Node topVboxleft() {
        return new VBox(
                new Label("Titel"),
                new Label("Autor"),
                new Label("Preis"),
                new Label("Format"),
                new Label("ISBN"));
    }

    private Node topVboxRight(BookModel bookModel) {

        VBox vBox = new VBox(
                new Label(bookModel.getTitleweb()),
                new Label(bookModel.getAuthorweb()),
                new Label(String.format("$%.2f", bookModel.getPriceusa())),
                new Label(bookModel.getFormatname()),
                new Label(bookModel.getIsbn())
        );
        vBox.setId("BookDetailVboxRight");
        return vBox;
    }

    private Node bottomTabPane(BookModel bookModel) {


        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);


        Tab imageTab = new Tab("Cover", imageView(bookModel.getImgUrl()));
        Tab keywordTab = new Tab("Datails", keywordText(bookModel.getKeyword()));

        tabPane.getTabs().add(imageTab);
        tabPane.getTabs().add(keywordTab);

        prefHeightProperty().bind(this.heightProperty());


        return tabPane;
    }

    private Node bottomTabPaneEmpty() {
        tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        Tab imageTab = new Tab("Cover", imageView());
        Tab keywordTab = new Tab("Details", keywordText());

        tabPane.getTabs().add(imageTab);
        tabPane.getTabs().add(keywordTab);

        return tabPane;
    }

    private Node imageView(String url) {
        VBox stackPane = new VBox();
        stackPane.setAlignment(Pos.CENTER);

        Image image = new Image(url);
        ImageView imageView = new ImageView(image);
        imageView.setPreserveRatio(true);

        stackPane.prefHeightProperty().bind(tabPane.heightProperty());
        imageView.fitHeightProperty().bind(stackPane.heightProperty().subtract(20));

        stackPane.getChildren().add(imageView);

        return stackPane;
    }

    private WebView keywordText(String s) {
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.loadContent("<html style=\"padding: 0 1em;\">" + s + "</html>");
        webView.prefWidthProperty().bind(this.widthProperty());

        return webView;
    }

    private ImageView imageView() {
        ImageView imageView = new ImageView();
        imageView.setPreserveRatio(true);
        return imageView;
    }

    private WebView keywordText() {
        WebView webView = new WebView();
        webView.prefWidthProperty().bind(this.widthProperty());
        return webView;
    }

}
