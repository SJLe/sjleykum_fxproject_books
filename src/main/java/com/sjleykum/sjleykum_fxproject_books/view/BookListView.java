package com.sjleykum.sjleykum_fxproject_books.view;

import com.sjleykum.sjleykum_fxproject_books.model.BookModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;

import java.util.List;

/**
 * ListView
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class BookListView extends BorderPane {
    private ListView<BookModel> listView;

    public BookListView(List<BookModel> bookModelList) {
        ObservableList<BookModel> bookModelObservableList = FXCollections.observableList(bookModelList);
        listView = new ListView<>(bookModelObservableList);
        listView.setId("BookListView");
        setCenter(listView);
    }

    public BookListView() {
        listView = new ListView<>();
        listView.setId("BookListView");
        setCenter(listView);
    }

    public ListView<BookModel> getListView() {
        return listView;
    }


}
