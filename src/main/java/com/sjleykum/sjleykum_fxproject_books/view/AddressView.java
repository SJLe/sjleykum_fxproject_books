package com.sjleykum.sjleykum_fxproject_books.view;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * AddressView
 *
 * @author Leykum, Salman Jona
 * @version 03.12.2021
 */
public class AddressView extends VBox {
    public TextField steetTextField;
    public TextField zipTextField;
    public TextField cityTextField;

    public AddressView() {
        setId("AddressView");
        getChildren().add(midHbox());
        getChildren().add(bottomHbox());
    }

    private Node midHbox() {
        HBox hBox = new HBox();
        hBox.getStyleClass().add("PaddingTopBottom0_2em");
        Label streetLabel = new Label("Straße/ Nr.");
        streetLabel.getStyleClass().add("AddressLabelLeft");
        hBox.getChildren().add(streetLabel);
        steetTextField = new TextField();
        steetTextField.getStyleClass().add("WideTextField");
        hBox.getChildren().add(steetTextField);
        return hBox;
    }

    private Node bottomHbox() {
        HBox hBox = new HBox();
        hBox.getStyleClass().add("PaddingTopBottom0_2em");
        Label zipLabel = new Label("Plz.");
        zipLabel.getStyleClass().add("AddressLabelLeft");
        hBox.getChildren().add(zipLabel);
        hBox.getChildren().add(zipTextField = new TextField());
        Label cityLabel = new Label("Ort");
        cityLabel.getStyleClass().add("AddressLabelRight");
        hBox.getChildren().add(cityLabel);
        cityTextField = new TextField();
        hBox.getChildren().add(cityTextField);
        return hBox;
    }

    public TextField getSteetTextField() {
        return steetTextField;
    }

    public TextField getZipTextField() {
        return zipTextField;
    }

    public TextField getCityTextField() {
        return cityTextField;
    }
}
