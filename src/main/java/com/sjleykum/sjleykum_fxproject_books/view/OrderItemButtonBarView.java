package com.sjleykum.sjleykum_fxproject_books.view;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 * OrderItemButtonBarView
 *
 * @author Leykum, Salman Jona
 * @version 03.12.2021
 */
public class OrderItemButtonBarView extends HBox {
    Button delBtn = new Button("- Artikel");

    public OrderItemButtonBarView () {
        getChildren().add(delBtn);
    }

    public Button getDelBtn() {
        return delBtn;
    }
}
