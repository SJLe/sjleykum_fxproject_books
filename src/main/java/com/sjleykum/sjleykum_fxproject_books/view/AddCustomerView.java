package com.sjleykum.sjleykum_fxproject_books.view;

import com.sjleykum.sjleykum_fxproject_books.CustomerGender;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * AddCustomerView
 *
 * @author Leykum, Salman Jona
 * @version 03.12.2021
 */
public class AddCustomerView extends VBox {

    private TextField firstnameTextField;
    private TextField lastnameTextField;
    private final AddressView billingAddressView = new AddressView();
    private final AddressView shippingAddressView = new AddressView();
    private final Button saveBtn = new Button("Speichern");
    private final Button cancelBtn = new Button("Abbrechen");
    private final Button deleteBtn = new Button("Löschen");

    final ToggleGroup genderToggleGroup = new ToggleGroup();
    RadioButton radioButtonFemale = new RadioButton();
    RadioButton radioButtonMale = new RadioButton();
    RadioButton radioButtonDiv = new RadioButton();

    public AddCustomerView() {
        setId("AddCustomerView");
        getChildren().add(topHbox());
        getChildren().add(bottomHbox());
        getChildren().add(new Label("Rechnungsadresse"));
        getChildren().add(billingAddressView);
        getChildren().add(new Label("Lieferadresse"));
        getChildren().add(shippingAddressView);
        getChildren().add(buttonBar());
    }

    private Node topHbox() {
        HBox hBox = new HBox();
        hBox.getStyleClass().add("PaddingTopBottom0_2em");
        hBox.getChildren().add(new Label("Vorname"));
        firstnameTextField = new TextField();
        hBox.getChildren().add(firstnameTextField);
        hBox.getChildren().add(new Label("Nachname"));
        lastnameTextField = new TextField();
        hBox.getChildren().add(lastnameTextField);
        return hBox;
    }

    private Node bottomHbox() {
        HBox hBox = new HBox();
        hBox.getStyleClass().add("PaddingTopBottom0_2em");
        radioButtonFemale.setText("Frau");
        radioButtonFemale.setUserData(CustomerGender.F);
        radioButtonMale.setText("Herr");
        radioButtonMale.setUserData(CustomerGender.M);
        radioButtonDiv.setText("Div");
        radioButtonDiv.setUserData(CustomerGender.D);
        radioButtonFemale.setToggleGroup(genderToggleGroup);
        radioButtonMale.setToggleGroup(genderToggleGroup);
        radioButtonDiv.setToggleGroup(genderToggleGroup);
        radioButtonDiv.setSelected(true);
        hBox.getChildren().addAll(
                new Label("Anrede"),
                radioButtonFemale,
                radioButtonMale,
                radioButtonDiv
        );
        return hBox;
    }

    private Node buttonBar() {
        HBox hBox = new HBox();
        hBox.setId("AddCustomerButtonBar");
        saveBtn.setDefaultButton(true);
        cancelBtn.setCancelButton(true);
        hBox.getChildren().addAll(saveBtn,cancelBtn,deleteBtn);
        return hBox;
    }

    public Button getSaveBtn() {
        return saveBtn;
    }

    public Button getCancelBtn() {
        return cancelBtn;
    }

    public TextField getFirstnameTextField() {
        return firstnameTextField;
    }

    public TextField getLastnameTextField() {
        return lastnameTextField;
    }

    public RadioButton getRadioButtonFemale() {
        return radioButtonFemale;
    }

    public RadioButton getRadioButtonMale() {
        return radioButtonMale;
    }

    public ToggleGroup getGenderToggleGroup() {
        return genderToggleGroup;
    }

    public AddressView getBillingAddressView() {
        return billingAddressView;
    }

    public AddressView getShippingAddressView() {
        return shippingAddressView;
    }

    public Button getDeleteBtn() {
        return deleteBtn;
    }
}
