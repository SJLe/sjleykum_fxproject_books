package com.sjleykum.sjleykum_fxproject_books.view;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * ConnectionView
 *
 * @author Leykum, Salman Jona
 * @version 07.12.2021
 */
public class ConnectionView extends BorderPane {
    private TextField urlTextField;
    private TextField databaseTextField;
    private TextField userTextField;
    private PasswordField passwordField;
    private final Button connectBtn = new Button("Verbinden");
    private final Button cancelBtn = new Button("Abbrechen");

    public ConnectionView() {
        setId("Connection");
        setCenter(centerHbox());
        setBottom(bottomButtonBar());
    }


    private Node centerHbox(){
        return new HBox(leftVBox(),rightVbox());
    }

    private Node leftVBox() {
        Label urlLabel = new Label("URL");
        Label databaseLabel = new Label("Datenbank");
        Label userLabel = new Label("Nutzername");
        Label passwordLabel = new Label("Passwort");
        return new VBox(urlLabel, databaseLabel, userLabel, passwordLabel);
    }

    private Node rightVbox() {
        urlTextField = new TextField("mysql://localhost:3306");
        urlTextField.setMinWidth(200);
        databaseTextField = new TextField("customer_data");
        userTextField = new TextField("root");
        passwordField = new PasswordField();
        return new VBox(urlTextField, databaseTextField, userTextField, passwordField);
    }

    private Node bottomButtonBar() {
        cancelBtn.setCancelButton(true);
        connectBtn.setDefaultButton(true);
        HBox hBox = new HBox(connectBtn, cancelBtn);
        hBox.getStyleClass().add("alignCenter");
        return hBox;
    }

    public TextField getUrlTextField() {
        return urlTextField;
    }

    public TextField getUserTextField() {
        return userTextField;
    }

    public PasswordField getPasswordField() {
        return passwordField;
    }

    public Button getConnectBtn() {
        return connectBtn;
    }

    public Button getCancelBtn() {
        return cancelBtn;
    }

    public TextField getDatabaseTextField() {
        return databaseTextField;
    }
}
