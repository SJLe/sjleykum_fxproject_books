package com.sjleykum.sjleykum_fxproject_books.view;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * SearchBarView
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class bookSearchBarView extends HBox {

    private TextField searchField = new TextField();
    private Button searchButton = new Button("Suchen");


    public bookSearchBarView() {
        getStyleClass().add("ButtonBar");
        getChildren().addAll(searchField,searchButton);
    }

    public TextField getSearchField() {
        return searchField;
    }

    public Button getSearchButton() {
        return searchButton;
    }
}
