package com.sjleykum.sjleykum_fxproject_books.model;

/**
 * BookModel
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class BookModel {
    private final String titleshort;
    private final String author;
    private final double priceusa;
    private final String isbn;
    private final String formatname;
    private final String imgUrl;
    private final String titleweb;
    private final String authorweb;
    private final String keyword;

    private int id_order;
    private int pk;

    public BookModel(String titleshort, String titleweb, String author, String authorweb, double priceusa, String isbn, String formatname, String imgUrl, String keyword) {
        this.titleshort = titleshort;
        this.titleweb = titleweb;
        this.author = author;
        this.authorweb = authorweb;
        this.priceusa = priceusa;
        this.isbn = isbn;
        this.formatname = formatname;
        this.imgUrl = imgUrl;
        this.keyword = keyword;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public int getPk() {
        return pk;
    }

    public int getId_order() {
        return id_order;
    }

    @Override
    public String toString() {
        return String.format("""
                %s
                %s
                %s
                """, titleweb, author, formatname);
    }

    public String getInvoiceSting() {
        return String.format("""
                        ISBN: %-20s
                        %-20s  %-20s
                        %-20s
                        """
                ,isbn, titleweb, author, formatname);
    }

    public String getTitleshort() {
        return titleshort;
    }

    public String getAuthor() {
        return author;
    }

    public double getPriceusa() {
        return priceusa;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getFormatname() {
        return formatname;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getTitleweb() {
        return titleweb;
    }

    public String getAuthorweb() {
        return authorweb;
    }

    public String getKeyword() {
        return keyword;
    }
}
