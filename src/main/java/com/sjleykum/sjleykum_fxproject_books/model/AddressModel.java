package com.sjleykum.sjleykum_fxproject_books.model;

/**
 * Address
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class AddressModel {
    private final String steet;
    private final String city;
    private final String zipcode;
    private final boolean isShippingAddress;


    public AddressModel(String steet, String zipcode, String city, boolean isShippingAddress) {
        this.steet = steet;
        this.zipcode = zipcode;
        this.city = city;
        this.isShippingAddress = isShippingAddress;
    }

    public String getSteet() {
        return steet;
    }

    public String getCity() {
        return city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public boolean isShippingAddress() {
        return isShippingAddress;
    }
}
