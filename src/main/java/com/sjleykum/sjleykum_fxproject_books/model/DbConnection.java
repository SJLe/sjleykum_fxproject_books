package com.sjleykum.sjleykum_fxproject_books.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DbConnection
 * <p>
 * singleton - design pattern
 *
 * @author Leykum, Salman Jona
 * @version 22.11.2021
 */
public class DbConnection {
    private static Connection connection;

//    private static final String SERVER = "localhost";
//    private static final String PORT = "3306";
//    private static final String DATABASE = "customerdata";
//    private static final String URL = "jdbc:mysql://" + SERVER + ':' + PORT + '/' + DATABASE;
    private static String URL = "";
//    private static String URL = "jdbc:mysql://localhost:3306/customerdata";
    private static String USER = "root";
    private static String PASSWORD = "";


    private DbConnection() {
    }

    public static Connection getInstance() throws SQLException{
        if (connection == null) {
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
        }
        return connection;
    }

    public static void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setURL(String URL) {
        DbConnection.URL = "jdbc:" + URL;
    }

    public static void setUSER(String USER) {
        DbConnection.USER = USER;
    }

    public static void setPASSWORD(String PASSWORD) {
        DbConnection.PASSWORD = PASSWORD;
    }
}
