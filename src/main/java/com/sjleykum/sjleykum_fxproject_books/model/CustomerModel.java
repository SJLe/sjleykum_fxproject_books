package com.sjleykum.sjleykum_fxproject_books.model;

import com.sjleykum.sjleykum_fxproject_books.CustomerGender;

/**
 * CustomerModel
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class CustomerModel {
    private int id_customer;
    private final String firstname;
    private final String lastname;
    private final CustomerGender customerGender;
    private AddressModel billingAddress;
    private AddressModel shippingAddress;

    public CustomerModel(String firstname, String lastname, CustomerGender customerGender) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.customerGender = customerGender;
    }

    public CustomerModel(int id_customer, String firstname, String lastname, CustomerGender customerGender) {
        this.id_customer = id_customer;
        this.firstname = firstname;
        this.lastname = lastname;
        this.customerGender = customerGender;
    }

    public int getId_customer() {
        return id_customer;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getCustomerGender() {
        return customerGender.toString();
    }

    public AddressModel getBillingAddress() {
        return billingAddress;
    }

    public AddressModel getShippingAddress() {
        return shippingAddress;
    }

    public void setBillingAddress(AddressModel billingAddress) {
        this.billingAddress = billingAddress;
    }

    public void setShippingAddress(AddressModel shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @Override
    public String toString() {
        return String.format("Kd.Nr.: %d %s, %s", id_customer, lastname, firstname);
    }

    public String getInvoiceString() {
        return String.format("""
                        Kd.Nr.: %d
                        %s %s
                        %s
                        %s %s
                                                
                                                
                        Lieferaddresse
                        %s
                        %s %s
                                        
                        """, id_customer, firstname, lastname
                , billingAddress.getSteet(), billingAddress.getZipcode(), billingAddress.getCity()
                , shippingAddress.getSteet(), shippingAddress.getZipcode(), shippingAddress.getCity());
    }
}
