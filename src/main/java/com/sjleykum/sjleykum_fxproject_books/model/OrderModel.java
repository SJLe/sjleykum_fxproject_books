package com.sjleykum.sjleykum_fxproject_books.model;

/**
 * OrderModel
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class OrderModel {
    private int id_order;
    private int id_customer;


    public OrderModel(int id_order, int id_customer) {
        this.id_order = id_order;
        this.id_customer = id_customer;
    }

    public int getId_order() {
        return id_order;
    }

    public int getId_customer() {
        return id_customer;
    }

    @Override
    public String toString() {
        return String.valueOf(id_order);
    }
}
