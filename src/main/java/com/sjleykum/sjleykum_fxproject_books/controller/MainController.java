package com.sjleykum.sjleykum_fxproject_books.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sjleykum.sjleykum_fxproject_books.CustomerGender;
import com.sjleykum.sjleykum_fxproject_books.view.*;
import com.sjleykum.sjleykum_fxproject_books.dao.*;
import com.sjleykum.sjleykum_fxproject_books.model.BookModel;
import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;
import com.sjleykum.sjleykum_fxproject_books.model.OrderModel;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * BookController
 *
 * @author Leykum, Salman Jona
 * @version 01.12.2021
 */
public class MainController {
    private Stage stage;
    private Scene scene;


    //DAO
    private JsonWebApiBookDAO jsonWebApiBookDAO = new JsonWebApiBookDAO();
    private CustomerDAO customerDAO = new SqlCustomerDAO();
    private OrderDAO orderDAO = new SqlOrderDAO();
    //

    private BorderPane borderPane;

    private BookListView bookListView;
    private com.sjleykum.sjleykum_fxproject_books.view.bookSearchBarView bookSearchBarView;

    private CustomerListView customerListView;
    private CustomerSearchBarView customerSearchBarView;
    private OrderListView orderListView;
    private BookListView orderBookListView;
    private OrderButtonBarView orderButtonBarView;
    private OrderItemButtonBarView orderItemButtonBarView;

    private Button addItemToOrderBtn = new Button("Zur Bestellung hinzufügen");
    private Button addCustomerBtn = new Button("+ Kunde");
    private Button customerDetailBtn = new Button("Details");


    private VBox vBoxRight;
    private VBox vBoxLeft;
    private HBox customerButtonBar;


    public MainController(Stage stage) {
        this.stage = stage;

        //pane
        borderPane = new BorderPane();


        //view
        bookSearchBarView = new bookSearchBarView();
        bookListView = new BookListView();
        customerSearchBarView = new CustomerSearchBarView();

        orderBookListView = new BookListView();

        orderButtonBarView = new OrderButtonBarView();

        customerDAO.readAllCustomers();
        customerListView = new CustomerListView(customerDAO.getCustomerModelList());

        orderListView = new OrderListView();
        orderItemButtonBarView = new OrderItemButtonBarView();

        borderPane.setCenter(new BookDetailView());

        customerButtonBar = new HBox(addCustomerBtn, customerDetailBtn);

        vBoxLeft = new VBox(bookSearchBarView, bookListView, addItemToOrderBtn);
        vBoxLeft.setId("MainWindowLeft");

        borderPane.setLeft(vBoxLeft);
        vBoxRight = new VBox(customerSearchBarView,   //0
                customerListView,                      //1
                customerButtonBar,                     //2
                new Label("Bestellungen"),         //3
                orderListView,                          //4
                orderButtonBarView,                     //5
                new Label("Items"),                 //6
                orderBookListView,                       //7
                orderItemButtonBarView                   //8
        );
        vBoxRight.setId("MainWindowRight");
        borderPane.setRight(vBoxRight);

        //events
        bookSearchBarView.getSearchButton().setOnMouseClicked(event -> searchButtonClicked());
        bookSearchBarView.getSearchField().setOnAction(event -> searchButtonClicked());
        addItemToOrderBtn.setOnMouseClicked(event -> addItemToOrderButtonClicked());
        customerSearchBarView.getCustomerSearchBtn().setOnMouseClicked(event -> customerSearchBtnClicked());
        customerSearchBarView.getCustomerSearchTextField().setOnAction(event -> customerSearchBtnClicked());
        addCustomerBtn.setOnMouseClicked(event -> addCustomerBtnClick());
        customerDetailBtn.setOnMouseClicked(event -> customerDetailBtnClicked());
        customerListView.getListView().setOnMouseClicked(event -> showCustomerOrder());
        orderButtonBarView.getPdfBtn().setOnMouseClicked(event -> pdfButtonClicked());


        //scene and stage
        scene = new Scene(borderPane, 1280, 720);
        scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle("Bookshop");
        stage.show();
    }

    private void pdfButtonClicked() {
        OrderModel orderModel = orderListView.getListView().getSelectionModel().getSelectedItem();

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extensionFilter);

        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            Document document = new Document();
            try {
                PdfWriter.getInstance(document, new FileOutputStream(file));
                document.open();
                Font font = FontFactory.getFont(FontFactory.COURIER, 11, BaseColor.BLACK);
                Font fontBold = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, BaseColor.BLACK);

                float[] colWidth = {180f, 20f};

                PdfPTable table = new PdfPTable(colWidth);
                table.setWidthPercentage(100);
                table.getDefaultCell().setBorder(0);

                double sum = 0;

                for (BookModel bookModel : orderDAO.getOrderItemList(orderModel)) {
                    table.addCell(bookModel.getInvoiceSting());
                    table.addCell(String.format("$%.2f", bookModel.getPriceusa()));
                    sum += bookModel.getPriceusa();
                }

                PdfPCell sumLabelCell = new PdfPCell(new Phrase("Summe", fontBold));
                sumLabelCell.setBorder(0);

                PdfPCell sumStringCell = new PdfPCell(new Phrase(String.format("$%.2f", sum), fontBold));

                sumStringCell.setBorder(0);

                table.addCell(sumLabelCell);
                table.addCell(sumStringCell);

                CustomerModel customerModel = customerListView.getListView().getSelectionModel().getSelectedItem();

                if (customerModel != null) {
                    Paragraph pAddress = new Paragraph(customerModel.getInvoiceString(), font);

                    document.add(pAddress);
                    document.add(table);
                }
            } catch (FileNotFoundException | DocumentException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Fehler");
                alert.setContentText(e.getMessage());
                alert.show();
            } finally {
                try {
                    document.close();
                } catch (Exception e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Fehler");
                    alert.setContentText("Kein Kunde Ausgewählt");
                    alert.show();
                }
            }
        }
    }

    private void customerDetailBtnClicked() {
        CustomerModel customerModel = customerListView.getListView().getSelectionModel().getSelectedItem();
        if (customerModel != null) {
            AddCustomerController addCustomerController = new AddCustomerController(customerModel);
            showCustomerDetails(addCustomerController, customerModel);

            addCustomerController.getStage().setOnHiding(event -> showAllCustomers());
        }
    }

    private void showCustomerDetails(AddCustomerController addCustomerController, CustomerModel customerModel) {
        addCustomerController.getAddCustomerView().getFirstnameTextField().setText(customerModel.getFirstname());
        addCustomerController.getAddCustomerView().getLastnameTextField().setText(customerModel.getLastname());
        if (customerModel.getCustomerGender().equals(CustomerGender.F.toString())) {
            addCustomerController.getAddCustomerView().getGenderToggleGroup().selectToggle(addCustomerController.getAddCustomerView().getRadioButtonFemale());
        } else if (customerModel.getCustomerGender().equals(CustomerGender.M.toString())) {
            addCustomerController.getAddCustomerView().getGenderToggleGroup().selectToggle(addCustomerController.getAddCustomerView().getRadioButtonMale());
        }
        if (customerModel.getBillingAddress() != null) {
            addCustomerController.getAddCustomerView().getBillingAddressView().getSteetTextField().setText(customerModel.getBillingAddress().getSteet());
            addCustomerController.getAddCustomerView().getBillingAddressView().getZipTextField().setText(customerModel.getBillingAddress().getZipcode());
            addCustomerController.getAddCustomerView().getBillingAddressView().getCityTextField().setText(customerModel.getBillingAddress().getCity());
        }
        if (customerModel.getShippingAddress() != null) {
            addCustomerController.getAddCustomerView().getShippingAddressView().getSteetTextField().setText(customerModel.getShippingAddress().getSteet());
            addCustomerController.getAddCustomerView().getShippingAddressView().getZipTextField().setText(customerModel.getShippingAddress().getZipcode());
            addCustomerController.getAddCustomerView().getShippingAddressView().getCityTextField().setText(customerModel.getShippingAddress().getCity());
        }
    }

    private void customerSearchBtnClicked() {
        String searchString = customerSearchBarView.getCustomerSearchTextField().getText();
        customerDAO.searchCustomer(searchString);
        showEmptyOrderList();
        updateCustomerListView();
    }

    private void addCustomerBtnClick() {
        AddCustomerController addCustomerController = new AddCustomerController(null);
        addCustomerController.getStage().setOnHiding(event -> showAllCustomers());
    }

    private void showAllCustomers() {
        customerDAO.readAllCustomers();
        updateCustomerListView();
    }

    private void updateCustomerListView() {
        customerListView = new CustomerListView(customerDAO.getCustomerModelList());
        vBoxRight.getChildren().set(1, customerListView);
        customerListView.getListView().setOnMouseClicked(event -> showCustomerOrder());
    }

    private void addItemToOrderButtonClicked() {
        BookModel bookModel = bookListView.getListView().getSelectionModel().getSelectedItem();
        OrderModel orderModel = orderListView.getListView().getSelectionModel().getSelectedItem();
        orderDAO.addItemToOrder(bookModel, orderModel);
        showOrderItems();
    }

    private void showDetail() {
        BookModel bookModel = bookListView.getListView().getSelectionModel().getSelectedItem();
        borderPane.setCenter(new BookDetailView(bookModel));
    }

    private void orderShowDetail() {
        BookModel bookModel = orderBookListView.getListView().getSelectionModel().getSelectedItem();
        if (bookModel != null) {
            borderPane.setCenter(new BookDetailView(bookModel));
        }
    }

    private void showCustomerOrder() {
        CustomerModel customerModel = customerListView.getListView().getSelectionModel().getSelectedItem();

        //events
        orderButtonBarView.getAddOrderBtn().setOnMouseClicked(event -> addOrderBtnClick(customerModel));

        if (!orderDAO.getOrderModelList(customerModel).isEmpty()) {
            orderListView = new OrderListView(orderDAO.getOrderModelList(customerModel));
            //set
            vBoxRight.getChildren().set(4, orderListView);
            resetOrderBookListView();

            orderListView.getListView().setOnMouseClicked(event -> showOrderItems());

        } else {
            showEmptyOrderList();
        }
    }

    private void showEmptyOrderList() {
        orderListView = new OrderListView();
        vBoxRight.getChildren().set(4, orderListView);
        resetOrderBookListView();
    }

    private void delOrderBtnClick(OrderModel orderModel) {
        orderDAO.deleteCustomerOrder(orderModel);
        showCustomerOrder();
    }

    private void orderItemDelBtnClicked(OrderModel orderModel) {
        BookModel bookModel = orderBookListView.getListView().getSelectionModel().getSelectedItem();
        orderDAO.deleteItemFromOrder(bookModel, orderModel);
        showOrderItems();
    }

    private void resetOrderBookListView() {
        orderBookListView = new BookListView();
        vBoxRight.getChildren().set(7, orderBookListView);
    }

    private void addOrderBtnClick(CustomerModel customerModel) {
        if (customerModel != null) {
            orderDAO.addOrder(customerModel);
            showCustomerOrder();
        }
    }

    private void showOrderItems() {
        OrderModel orderModel = orderListView.getListView().getSelectionModel().getSelectedItem();

        orderButtonBarView.getDelOrderBtn().setOnMouseClicked(event -> delOrderBtnClick(orderModel));

        if (!orderDAO.getOrderItemList(orderModel).isEmpty()) {
            orderBookListView = new BookListView(orderDAO.getOrderItemList(orderModel));
            //set
            vBoxRight.getChildren().set(7, orderBookListView);

            //event
            orderBookListView.getListView().setOnMouseClicked(event -> orderShowDetail());
            orderItemButtonBarView.getDelBtn().setOnMouseClicked(event -> orderItemDelBtnClicked(orderModel));

        } else {
            orderBookListView = new BookListView();
            vBoxRight.getChildren().set(7, orderBookListView);
        }
    }

    private void searchButtonClicked() {
        String searchSting = "";
        searchSting = bookSearchBarView.getSearchField().getText();
        searchSting = searchSting.replace(' ', '+');
        bookListView = new BookListView(jsonWebApiBookDAO.searchTitles(searchSting));
        //event
        bookListView.getListView().setOnMouseClicked(event -> showDetail());
        if (jsonWebApiBookDAO.searchTitles(searchSting).isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Nichts gefunden");
            alert.setTitle("Nichts gefunden");
            alert.show();
        }
        vBoxLeft.getChildren().set(1, bookListView);
    }


}
