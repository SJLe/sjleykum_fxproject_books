package com.sjleykum.sjleykum_fxproject_books.controller;

import com.sjleykum.sjleykum_fxproject_books.view.ConnectionView;
import com.sjleykum.sjleykum_fxproject_books.model.DbConnection;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * ConnectionController
 *
 * @author Leykum, Salman Jona
 * @version 07.12.2021
 */
public class ConnectionController {
    private Stage stage;
    private Scene scene;
    ConnectionView connectionView = new ConnectionView();

    private BorderPane borderPane;

    public ConnectionController() {
        stage = new Stage();
        borderPane = new BorderPane();

        borderPane.setCenter(connectionView);

        //events
        connectionView.getCancelBtn().setOnMouseClicked(event -> System.exit(0));
        connectionView.getConnectBtn().setOnMouseClicked(event -> connectBtnClicked());
        connectionView.getPasswordField().setOnAction(event -> connectBtnClicked());
        connectionView.getUrlTextField().setOnAction(event -> connectBtnClicked());
        connectionView.getUserTextField().setOnAction(event -> connectBtnClicked());
        connectionView.getDatabaseTextField().setOnAction(event -> connectBtnClicked());


        scene = new Scene(borderPane);
        scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
        stage.setTitle("Verbinden");
        stage.setScene(scene);
        stage.showAndWait();
    }

    private void connectBtnClicked() {
        String url = connectionView.getUrlTextField().getText();
        String user = connectionView.getUserTextField().getText();
        String password = connectionView.getPasswordField().getText();
        String databaseName = connectionView.getDatabaseTextField().getText();
        DbConnection.setURL(url);
        DbConnection.setUSER(user);
        DbConnection.setPASSWORD(password);
        try {
            Connection connection = DbConnection.getInstance();
            String sqlCreateDatabase = "CREATE DATABASE IF NOT EXISTS " + databaseName;
            String sqlUseDatabase = "USE `"+ databaseName +"`";
            String sqlCreateTableAddress = "CREATE TABLE IF NOT EXISTS `address` (" +
                    "  `id_address` int(11) NOT NULL AUTO_INCREMENT," +
                    "  `FK_customer_id` int(10) unsigned NOT NULL," +
                    "  `street` varchar(255) DEFAULT NULL," +
                    "  `city` varchar(255) DEFAULT NULL," +
                    "  `zipCode` varchar(255) DEFAULT NULL," +
                    "  `isShippingAddress` tinyint(3) unsigned DEFAULT NULL," +
                    "  PRIMARY KEY (`id_address`)," +
                    "  KEY `FK_customer_id` (`FK_customer_id`)," +
                    "  CONSTRAINT `FK_customer_id` FOREIGN KEY (`FK_customer_id`) REFERENCES `customers` (`id_customer`) ON DELETE CASCADE ON UPDATE CASCADE" +
                    ") ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4;";

            String sqlCreateTableBooks = "CREATE TABLE IF NOT EXISTS `books` (" +
                    "  `pk` int(10) unsigned NOT NULL AUTO_INCREMENT," +
                    "  `id_order` int(10) unsigned NOT NULL," +
                    "  `isbn` varchar(13) NOT NULL," +
                    "  `titleshort` varchar(255) NOT NULL," +
                    "  `titleweb` varchar(255) NOT NULL," +
                    "  `author` varchar(255) DEFAULT NULL," +
                    "  `authorweb` varchar(255) DEFAULT NULL," +
                    "  `priceusa` double NOT NULL DEFAULT 0," +
                    "  `formatname` varchar(255) DEFAULT NULL," +
                    "  `imgUrl` varchar(255) NOT NULL," +
                    "  `keyword` text DEFAULT NULL," +
                    "  PRIMARY KEY (`pk`)," +
                    "  KEY `id_order` (`id_order`)," +
                    "  CONSTRAINT `id_order` FOREIGN KEY (`id_order`) REFERENCES `customer_order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE" +
                    ") ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4;";

            String sqlCreateTableCustomers = "CREATE TABLE IF NOT EXISTS `customers` (" +
                    "  `id_customer` int(10) unsigned NOT NULL AUTO_INCREMENT," +
                    "  `firstname` varchar(255) DEFAULT NULL," +
                    "  `lastname` varchar(255) NOT NULL," +
                    "  `gender` varchar(1) NOT NULL," +
                    "  PRIMARY KEY (`id_customer`)" +
                    ") ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;";

            String sqlCreateTableCustomerOrder = "CREATE TABLE IF NOT EXISTS `customer_order` (" +
                    "  `id_order` int(10) unsigned NOT NULL AUTO_INCREMENT," +
                    "  `FK_id_customer` int(10) unsigned NOT NULL DEFAULT 0," +
                    "  PRIMARY KEY (`id_order`)," +
                    "  KEY `FK_id_customer` (`FK_id_customer`)," +
                    "  CONSTRAINT `FK_id_customer` FOREIGN KEY (`FK_id_customer`) REFERENCES `customers` (`id_customer`) ON DELETE CASCADE ON UPDATE CASCADE" +
                    ") ENGINE=InnoDB AUTO_INCREMENT=1021 DEFAULT CHARSET=utf8mb4;";


            try (Statement statement = connection.createStatement()) {
                statement.execute(sqlCreateDatabase);
            }
            try (Statement statement = connection.createStatement()) {
                statement.execute(sqlUseDatabase);
            }
            try (Statement statement = connection.createStatement()) {
                statement.execute(sqlCreateTableCustomers);
            }
            try (Statement statement = connection.createStatement()) {
                statement.execute(sqlCreateTableAddress);
            }
            try (Statement statement = connection.createStatement()) {
                statement.execute(sqlCreateTableCustomerOrder);
            }
            try (Statement statement = connection.createStatement()) {
                statement.execute(sqlCreateTableBooks);
            }

            stage.close();
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.show();
        }

    }
}
