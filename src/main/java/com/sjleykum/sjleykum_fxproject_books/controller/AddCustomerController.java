package com.sjleykum.sjleykum_fxproject_books.controller;

import com.sjleykum.sjleykum_fxproject_books.CustomerGender;
import com.sjleykum.sjleykum_fxproject_books.view.AddCustomerView;
import com.sjleykum.sjleykum_fxproject_books.dao.AddressDAO;
import com.sjleykum.sjleykum_fxproject_books.dao.CustomerDAO;
import com.sjleykum.sjleykum_fxproject_books.dao.SqlAddressDAO;
import com.sjleykum.sjleykum_fxproject_books.dao.SqlCustomerDAO;
import com.sjleykum.sjleykum_fxproject_books.model.AddressModel;
import com.sjleykum.sjleykum_fxproject_books.model.CustomerModel;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.Optional;

/**
 * AddCustomerController
 *
 * @author Leykum, Salman Jona
 * @version 03.12.2021
 */
public class AddCustomerController {
    private final Stage stage;


    private final AddCustomerView addCustomerView;
    private final CustomerDAO customerDAO = new SqlCustomerDAO();
    private final AddressDAO addressDAO = new SqlAddressDAO();


    public AddCustomerController(CustomerModel updateCustomer) {
        stage = new Stage();
        BorderPane borderPane = new BorderPane();

        addCustomerView = new AddCustomerView();

        borderPane.setCenter(addCustomerView);

        //events
        addCustomerView.getSaveBtn().setOnMouseClicked(event -> saveBtnClicked(updateCustomer));
        addCustomerView.getCancelBtn().setOnMouseClicked(event -> stage.close());
        addCustomerView.getDeleteBtn().setDisable(updateCustomer == null);
        addCustomerView.getDeleteBtn().setOnMouseClicked(event -> deleteBtnClick(updateCustomer));


        Scene scene = new Scene(borderPane);
        scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    private void deleteBtnClick(CustomerModel customerModel) {
        if (customerModel != null) {
            Alert confirmationAlert = new Alert(Alert.AlertType.CONFIRMATION, "Wirklich Löschen?");
            Optional<ButtonType> result = confirmationAlert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                customerDAO.deleteCustomer(customerModel);
            }
        }
        stage.close();
    }


    private void saveBtnClicked(CustomerModel customerModel) {
        if (customerModel == null) {
            int pk = customerDAO.addCustomer(getNewCustomer());

            addressDAO.addAddress(getNewBillingAddress(),pk);
            addressDAO.addAddress(getNewShippingAddress(),pk);
        } else {
            customerDAO.updateCustomer(customerModel, getNewCustomer());
            addressDAO.updateAddress(customerModel.getBillingAddress(), getNewBillingAddress(),customerModel, false);
            addressDAO.updateAddress(customerModel.getShippingAddress(), getNewShippingAddress(),customerModel, true);
        }
        stage.close();
    }

    private CustomerModel getNewCustomer() {
        CustomerGender customerGender = (CustomerGender) addCustomerView.getGenderToggleGroup().getSelectedToggle().getUserData();
        return new CustomerModel(
                //pk
                addCustomerView.getFirstnameTextField().getText(),
                addCustomerView.getLastnameTextField().getText(),
                customerGender
        );
    }

    private AddressModel getNewBillingAddress() {
        return new AddressModel(
                addCustomerView.getBillingAddressView().getSteetTextField().getText(),
                addCustomerView.getBillingAddressView().getZipTextField().getText(),
                addCustomerView.getBillingAddressView().getCityTextField().getText(),
                false
        );
    }

    private AddressModel getNewShippingAddress() {
        return new AddressModel(
                addCustomerView.getShippingAddressView().getSteetTextField().getText(),
                addCustomerView.getShippingAddressView().getZipTextField().getText(),
                addCustomerView.getShippingAddressView().getCityTextField().getText(),
                true
        );
    }

    public Stage getStage() {
        return stage;
    }

    public AddCustomerView getAddCustomerView() {
        return addCustomerView;
    }
}
