module com.sjleykum.sjleykum_fxproject_books {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.json;
    requires java.sql;
    requires itextpdf;
    requires pdfbox;
    requires javafx.web;


    opens com.sjleykum.sjleykum_fxproject_books to javafx.fxml;
    exports com.sjleykum.sjleykum_fxproject_books;
    exports com.sjleykum.sjleykum_fxproject_books.view;
    opens com.sjleykum.sjleykum_fxproject_books.view to javafx.fxml;
    exports com.sjleykum.sjleykum_fxproject_books.model;
    opens com.sjleykum.sjleykum_fxproject_books.model to javafx.fxml;
    exports com.sjleykum.sjleykum_fxproject_books.dao;
    opens com.sjleykum.sjleykum_fxproject_books.dao to javafx.fxml;
    exports com.sjleykum.sjleykum_fxproject_books.controller;
    opens com.sjleykum.sjleykum_fxproject_books.controller to javafx.fxml;
}